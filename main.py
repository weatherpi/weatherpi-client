from time import sleep
from weatherpi.weather_db import db_manager
from weatherpi.weatherstation import weatherstation
from weatherpi.logger import Logger


logger = Logger("Main")


def main(wait):
    """Main Weatherstation Function."""
    logger.info("Start weatherpi service.")
    dbm = db_manager(
        engine="mysql",
        db_path="weather_pi:password@maxsac.duckdns.org:2802/weather_data",
    )
    dbm.login_owner()
    wst = weatherstation()

    while True:
        data_id = dbm.add_weather(*wst.load_data())
        wst.load_picture(data_id)
        sleep(wait)


if __name__ == "__main__":
    main(1800)
