import sqlalchemy
from sqlalchemy import (
    create_engine,
    Column,
    Float,
    Integer,
    String,
    Boolean,
    ForeignKey,
    DateTime,
    func,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from datetime import datetime
from weatherpi.logger import Logger
from time import sleep
import yaml
import os.path

logger = Logger("DB Manager")
config = yaml.load(open(os.path.expanduser('~/.weatherpiconfig')))

Base = declarative_base()

class Weatherpi(Base):
    __tablename__ = "weatherpi"
    id = Column(Integer, primary_key=True)
    owner = Column(String(32))
    location = Column(String(32))
    weather_data = relationship("Weather_data", backref="weatherpi")
    # weather_pic = relationship("Weather_pic", backref="weatherpi")

class Weather_data(Base):
    __tablename__ = "weather_data"
    id = Column(Integer, primary_key=True)
    pi_id = Column(Integer, ForeignKey("weatherpi.id"))
    temperature_1 = Column(Float)
    temperature_2 = Column(Float)
    humidity = Column(Float)
    pressure = Column(Float)
    picture = Column(Boolean)
    time = Column(DateTime(timezone=True))
    weather_pic = relationship("Weather_pic", backref="weather_data")


class Weather_pic(Base):
    __tablename__ = "weather_pic"
    id = Column(Integer, primary_key=True)
    data_id = Column(Integer, ForeignKey("weather_data.id"))
    cloud_class = Column(String(32))


class db_manager:
    def __init__(self, db_path="/production/weather.db", engine="mysql", pp_ping=True):
        engine = create_engine("{}://{}".format(engine, db_path), pool_pre_ping=pp_ping)
        Session = sessionmaker(bind=engine)
        self.session = Session()
        # Create Tables defined earlier (Base-classes),
        # not necessary when they already exist:
        # Base.metadata.create_all(engine)
        self.pi_id = None

    def add_pi(self, owner, location):
        wp = Weatherpi(owner=owner, location=location)
        self.session.add(wp)
        try:
            self.session.commit()
        except sqlalchemy.exc.OperationalError as e:
            logger.warn("{e}, rolling back".format(e=e))
            self.session.rollback()

    def add_weather(self, temp1, temp2, hum, press):
        wd = Weather_data(
            pi_id=self.pi_id,
            temperature_1=temp1,
            temperature_2=temp2,
            humidity=hum,
            pressure=press,
            time=datetime.now(),
            picture = False,
        )
        self.session.add(wd)
        try:
            self.session.commit()
        except sqlalchemy.exc.OperationalError as e:
            logger.warn("{e}, rolling back".format(e=e))
            self.session.rollback()
        return wd.id

    # def add_pic(self, data_id):
    #     wp = Weather_pic(
    #             data_id = data_id,
    #             cloud_class = 'not labeled',
    #             )
    #     self.session.add(wp)
    #     self.session.commit()

    def search_owner(self, name):
        wp_owner = (
            self.session.query(Weatherpi).filter_by(owner=name).one()
        )
        return wp_owner.id

    # def search_weather(self, params, user="all", time=None):
    #     Params = [*params, 'pi_id', 'time'] # add time and pi_id for searching
    #     sql_params = [getattr(Weather_data, x) for x in Params]
    #     if(user == 'all'):
    #         wd = self.session.query(*sql_params)
    #     else:
    #         wd = self.session.query(*sql_params).filter(
    #              Weather_data.pi_id == user
    #              )
    #     if(time == None):
    #         time = wd.order_by(Weather_data.time)[-1].time
    #     wd = wd.filter(Weather_data.time >= time)
    #     return wd

    # i think this should be location to pi id
    def owner_to_pi_id(self, loc):
        wp_id = (
            self.session.query(Weatherpi).filter_by(location=loc).one()
        )
        return wp_id.id

    def login_owner(self):
        try:
            user = config['USER']
            self.pi_id = self.search_owner(user)
            logger.info("Successfull logged user {} in.".format(user))
        except sqlalchemy.orm.exc.NoResultFound as err:
            logger.warn("User {} does not exist. You have to add it.".format(user))
            add = input("Do you want to add it? (y/n): ")
            if add == "y":
                self.add_pi(user, config['LOCATION'])
                self.login_owner()
            else:
                raise err
        except sqlalchemy.exc.OperationalError as e:
            n = 60
            logger.warn("Cloud not log in, trying again in {} seconds".format(n))
            sleep(n)
            self.login_owner()
        except Exception as e:
            logger.critical(e)

    # def get_weather(self, params, user="all", time=None):
    #     wd = self.search_weather(params, user=user, time=time)
    #     Data = {}
    #     for param in params:
    #         Data[param] = [getattr(x, param) for x in wd]
    #     return Data

    # def get_locations(self):
    #     wp_loc = self.session.query(Weatherpi.location).all()
    #     return [[x.location] for x in wp_loc]

    def validate_pic(self, data_id):
        wd = (self.session.query(Weather_data).filter(
            Weather_data.id == data_id
        ).one())
        self.add_pic(data_id)
        wd.picture = True
        try:
            self.session.commit()
        except sqlalchemy.exc.OperationalError as e:
            logger.warn("{e}, rolling back".format(e=e))
            self.session.rollback()
        logger.info('Picture {} has been verified'.format(data_id))

def main():
    dbm = db_manager(engine= 'mysql', db_path='weather_pi:password@maxsac.duckdns.org:2802/weather_data')
    # dbm.login_owner('Maximilian')
    # dbm.add_weather(20,20,75,1000)
    # dbm.add_pic(3)
    # dbm.validate_pic(60)
    # lo

if __name__ == '__main__':
    main()
