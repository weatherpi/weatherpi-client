import Adafruit_DHT as dht
from weatherpi.error import SensorError


class hum_sens:
    def __init__(self, gpio=4):
        self.sensor = dht.DHT22
        self.gpio = gpio

    def get_data(self):
        """ Get the data form DHT22 sensor 
        and return a tuple.
        Returns: temp
        temp: float
            tempreature rounded to 1 digit
        """
        hum, _ = dht.read(self.sensor, self.gpio)
        if hum == None:
            raise SensorError(self.gpio)
        return round(hum, 1)
