from Adafruit_BMP import BMP085 as BMP
from weatherpi.error import SensorError


class pres_sens:
    def __init__(self):
        self.gpio = "2 & 3"
        self.sensor = BMP.BMP085()

    def get_data(self):
        """ Get the data from BMP sensor 
        and return a float.
        Returns: pres
        pres: float
            pressure not cleaned from height in hPa
        """
        pres = self.sensor.read_pressure()
        if pres == None:
            raise SensorError(self.gpio)
        return round(pres / 100)
