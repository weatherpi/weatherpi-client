from gpiozero import LED
from time import sleep


class led:
    def __init__(self, gpioG=16, gpioR=21):
        self.ledG = LED(gpioG)
        self.ledR = LED(gpioR)

    def blink(self, color):
        """ let LED blink to give consumer a feedback if
        data are taken.
        Returns:

        """
        if color == "r":
            self.ledR.on()
            sleep(1)
            self.ledR.off()
        elif color == "g":
            self.ledG.on()
            sleep(1)
            self.ledG.off()
        else:
            # here is place for a led error
            pass
