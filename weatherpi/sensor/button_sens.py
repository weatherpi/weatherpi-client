from gpiozero import Button
from time import sleep


class button_sens:
    def __init__(self, gpio=17):
        self.button = Button(gpio)
        self.gpio = gpio

    def trigger(self, func):
        """ If button is pressed func is
        executed """
        self.button.when_pressed = func


def main():
    but = button_sens()
    while True:

        def hello():
            print("Hello World!")

        but.trigger(hello)


if __name__ == "__main__":
    main()
