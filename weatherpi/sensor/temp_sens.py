import Adafruit_DHT as dht
from Adafruit_BMP import BMP085 as BMP
from weatherpi.error import SensorError


class temp_sens:
    def __init__(self, gpio=4):
        self.sensor1 = dht.DHT22
        self.sensor2 = BMP.BMP085()
        self.gpio1 = gpio
        self.gpio2 = "2 & 3"

    def get_data(self):
        """ Get the data form DHT22 sensor 
        and return a tuple.
        Returns: temp
        temp: float
            tempreature rounded to 1 digit
        """

        _, temp1 = dht.read(self.sensor1, self.gpio1)
        if temp1 == None:
            raise SensorError(self.gpio1)
        temp2 = self.sensor2.read_temperature()
        if(temp2 == None):
            raise SensorError(self.gpio2)
        return round(temp1, 1), round(temp2, 1)


def main():
    tem = temp_sens()
    print(tem.get_data())


if __name__ == "__main__":
    main()
