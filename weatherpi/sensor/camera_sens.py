import picamera.array as camarray
from picamera import PiCamera
from time import sleep
import os
import numpy as np
from PIL import Image
from io import BytesIO
import socket

from weatherpi.error import NoConnectionError, TooDarkWarning
from weatherpi.logger import Logger
logger = Logger("Camera Sensor")


class camera_sens:
    def __init__(self, resolution=[1024, 768], threshold=70):
        self.threshold = threshold
        self.resolution = resolution

    # def get_local_picture(self, path="./production/pictures/", name=None):
    #     """Get picture and save locally.

    #     Returns
    #     -------
    #     bool
    #         Wether picture is saved ==> bright enough.
    #     """
    #     if not os.path.exists(path):
    #         os.makedirs(path)

    #     if name is None:
    #         # name = path + pendulum.now(self.tz).to_w3c_string() + ".jpg"
    #         name = path + datetime.now().to_w3c_string() + ".jpg"

    #     with picamera.PiCamera(resolution=self.resolution) as camera:
    #         # ! This is a dirty hack for the camera to have enough time to set exposure ! #
    #         sleep(2)
    #         with camarray.PiRGBArray(camera) as output:
    #             camera.capture(output, "rgb")

    #             if np.mean(output.array) > self.threshold:
    #                 im = Image.fromarray(output.array)
    #                 im.save(name)

    def get_byte_picture(self):
        with PiCamera(resolution=self.resolution) as camera:
            # ! This is a dirty hack for the camera to have enough time to set exposure ! #
            sleep(2)
            with camarray.PiRGBArray(camera) as output:
                camera.capture(output, "rgb")

                if np.mean(output.array) < self.threshold:
                    raise TooDarkWarning

                im = Image.fromarray(output.array)

                byte_io = BytesIO()
                im.save(byte_io, 'jpeg')
                return byte_io


    def send_package(self, byte_data, data_id, host='maxsac.duckdns.org', port=2804):
        client = socket.socket()
        client.connect((host, port))
        package = client.makefile('wb')

        # Send picture and name
        package.write(byte_data.getvalue())
        #package.flush()
        package.write(str.encode(data_id))
        #package.flush()

        package.close()
        client.close()
        logger.info('Sending picture completed.')
        return True
    
    def get_picture(self, data_id, host='maxsac.duckdns.org', port=2804):
        try:
            byte_data = self.get_byte_picture()
            self.send_package(byte_data, data_id, host, port)
        except TooDarkWarning:
            logger.warn('No picture taken because it is to dark.')
        except ConnectionRefusedError as e:
            logger.error("{e}, make sure picture listener is listening".format(e=e))
        except TimeoutError as e:
            logger.error(e)
        except OSError as e:
            logger.error("{e}. Could not send picture. look further into this at `weatherpi/sensor/camera_sens.py` line 68.".format(e=e))
        except Exception as e:
            logger.critical(e)
        

if __name__ == "__main__":
    cam = camera_sens()
    cam.get_local_picture("./", "pipic.jpg")
    print(cam.get_picture())
