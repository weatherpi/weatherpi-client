from weatherpi.sensor.temp_sens import temp_sens
from weatherpi.sensor.pres_sens import pres_sens
from weatherpi.sensor.hum_sens import hum_sens
from weatherpi.sensor.camera_sens import camera_sens

# from weatherpi.sensor.led import led
from datetime import datetime
from weatherpi.error import *
from time import sleep
from weatherpi.logger import Logger
logger = Logger("Weatherstation")


class weatherstation:
    def __init__(self):
        self.tsensor = temp_sens()
        self.psensor = pres_sens()
        self.hsensor = hum_sens()
        self.camera = camera_sens(threshold=75)
        # self.led = led()
        self.tries = 0
        self.session = None
        self.pi = None

    def load_data(self):
        try:
            temp1, temp2 = self.tsensor.get_data()
            pres = self.psensor.get_data()
            hum = self.hsensor.get_data()
            time = datetime.now()
            self.tries = 0
            # self.led.blink('g')
            logger.info("temp1: {}, temp2: {}, hum: "
                    "{}, pres: {}".format( temp1, temp2, hum, pres))
            return temp1, temp2, hum, pres  # , time
        except SensorError as err:
            if self.tries <= 15:
                # self.led.blink('r')
                self.tries += 1
                sleep(1)
                return self.load_data()
            else:
                raise err
        except Exception as e:
            logger.critical(e)

    def load_picture(self, data_id):
        if data_id is not None:
            data_id = str(data_id).zfill(8)
            self.camera.get_picture(data_id)
