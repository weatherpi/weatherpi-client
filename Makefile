all: install bootscript.sh ~/.weatherpiconfig
	bash installation/install_information.sh


install: venv
	bash installation/install.sh

venv:
	python3 -m venv venv/weatherpi-client

bootscript.sh:
	sed "s#insert_weatherpipath#$$(pwd)#" installation/bootscript.sh > bootscript.sh

~/.weatherpiconfig: config.yml
	ln -sf $$(pwd)/$< $@

config.yml:
	bash installation/configure.sh

clean:
	rm -rf venv bootscript.sh config.yml ~/.weatherpiconfig


.PHONY: clean install
