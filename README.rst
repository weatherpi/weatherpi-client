.. image:: https://readthedocs.org/projects/weatherpi/badge/?version=latest

weatherpi-client
================

A weatherstation running on Raspberry Pis.

To install the weatherstation locally run these commands:

.. code::

    git clone https://gitlab.com/weatherpi/weatherpi-client.git --depth=1
    cd weatherpi-client
    make

Follow the steps provided in the install scripts, which will be prompted to your command line.


These commands will 

1. Create a virtual environment
2. Source the virtual environment and install the ``weatherpi`` package and all dependend packages using ``pip``
3. Create a ``config.yml`` file for the user with prompted name and location
4. Create a ``bootscript.sh`` and provide instructions to start the weatherstation at every reboot of the Raspberry Pi
