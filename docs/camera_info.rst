Camera Info
===========
Goal is to make a picture of the sky with a camera, to forecast the clouds.
Maybe is it posibile to extract further information from nature.

Installation of the camera module is quite simple.
First start the **raspi-config** and configure a connection to a peripher::

    sudo raspi-config

Then activate the camera and after rebooting your device you are able to make a testshot::

    raspistill -v -o test.jpg
