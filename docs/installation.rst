Installation
============

Download
--------

To install the watherstation on a raspberry pi simply download the git repository ::

    git clone https://gitlab.com/weatherpi/weatherpi-client.git --depth=1

Using ``--depth==1`` only downloads the last committed version and saves about 29MB diskspace (and
thus downloads faster).

Installation process
--------------------

To install the software simply running::

    make

is enough!
This goes through the following steps:

1. Create an virtual environment using ``python3 -m venv venv/weatherpi-client``
2. Source this virtual environment to install packages in it
3. Install the ``weatherpi-client`` package and all needed requirements using ``pip``
4. Copy the config file and bootscript to the right places and while doing this, insert the
   current path to the according place in the config file.
   You will be asked for your name and your location, which will be sent to the data server
5. Gives you the direction to add the bootscript to ``crontab`` to start the weatherpi-client with
   every reboot of the raspberry pi

Running
-------

There are three ways to start the weatherstation:

- When the above steps were followed, simply rebooting the raspberry pi is enough
- Sourcing the environment (``source venv/weatherpi-client/bin/activate``) and starting the main
  script with ``python main.py``
- Running ``bash bootscript.sh`` will run the activate the environment, start the main script and
  keep the logs in a temporary file (``/tmp/weatherpi.log``)


.. To emulate GPIO pins on everything not being a Raspberry Pi, use::

..     GPIOZERO_PIN_FACTORY=mock python

.. MariaDB
.. -------

.. .. code::

..     sudo apt-get install mariadb-client

.. Numpy
.. -----
.. To use ``numpy`` with virtual environment run::

..     sudo apt-get install libatlas-base-dev

.. Packages
.. --------

.. DHT22 Sensor
.. ~~~~~~~~~~~~
.. To use the DHT22 sensor, install adafruit package::

..     git clone https://github.com/hexdump42/Adafruit_Python_DHT
..     cd Adafruit_Python_DHT
..     pip install . --user

.. BMP180 Sensor
.. ~~~~~~~~~~~~~
.. To use the BMP180 sensor, enable I2C. I2C is a very commonly used standard
.. designed to allow one chip to talk to another::

..     sudo raspi-config

.. under `5 - Interfacing Options` is the possibilty to enable I2C. Afterwards install it
.. equavilent as DHT22 Sensor::

..     git clone https://github.com/adafruit/Adafruit_Python_BMP.git


.. MariaDB
.. ~~~~~~~
.. To install mariadb to start a mysql server, run::

..     sudo apt-get install mariadb-server

.. Pipenv
.. ~~~~~~
.. We are using pipenv as package dependency and virtual environment solver.
.. To install pipenv using python3 and adding minimal dependencies run::

..     sudo apt install python3-pip
..     pip3 install pipenv

.. and add pipenv to your path, i.e.::

..     echo 'export PATH="$HOME/.local/bin:$PATH"' >> $HOME/.bashrc
..     source $HOME/.bashrc

.. Now running `pipenv install` will add a new virtual environment and install all dependencies.
.. Run scripts using `pipenv run python <script.py>` or spawn a virtual environment shell with `pipenv shell`.

.. Install new packages with::

..     pipenv install <package>

.. and our own package for experimental use with::

..     pipenv run pip install -e .

.. venv
.. ~~~~
.. As an alternative to `pipenv` it is adviced to create an virtual environment using::

..     python3 -m venv venv/weatherpi

.. and activate it with::

..     source venv/weatherpi/bin/activate

.. Then install the required packages with::

..     pip install -r requirements.txt

.. To deactivate the environment run::

..     deactivate
