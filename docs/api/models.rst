Models
------

.. automodule:: predictions.models.neuralnet
   :noindex:
   :members:
   :private-members:
   :special-members: __init__
   :undoc-members:

.. automodule:: predictions.models.randomforest
   :noindex:
   :members:
   :private-members:
   :special-members: __init__
   :undoc-members:

.. automodule:: predictions.models.evaluate
   :noindex:
   :members:
   :private-members:
   :special-members: __init__
   :undoc-members:
