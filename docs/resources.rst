Resources
=========

Collection of links to learn about the components of the weatherstation.

- `GPIO Pins`_
- `Camera Package`_
- `RFID`_
- `Face Recognition Python`_
- `Rest Api Eve`_
- `Django Tutorial`_
- `Sql Alchemy`_


.. _GPIO Pins: https://www.raspberrypi.org/documentation/usage/gpio-plus-and-raspi2/README.md
.. _camera package: http://picamera.readthedocs.io/
.. _RFID: https://pimylifeup.com/raspberry-pi-rfid-rc522/
.. _Face Recognition Python: https://github.com/ageitgey/face_recognition
.. _Rest Api Eve: http://python-eve.org/
.. _Django tutorial: http://books.agiliq.com/projects/django-api-polls-tutorial/en/latest/
.. _Sql Alchemy: https://www.pythoncentral.io/overview-sqlalchemys-expression-language-orm-queries/
