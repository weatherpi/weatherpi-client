#!/bin/bash

# Gets overwritten with Makefile
WEATHERPI_PATH="insert_weatherpipath"
LOGFILE="$HOME/.weatherpi_history"

echo "Started bootscript" >> $LOGFILE

# Activate Virtual Environment
source "${WEATHERPI_PATH}/venv/weatherpi-client/bin/activate" 2>> $LOGFILE
echo "Activated Virtual Environment" >> $LOGFILE

# Start WeatherPi Service
python "${WEATHERPI_PATH}/main.py"

echo "Main stopped." >> $LOGFILE
