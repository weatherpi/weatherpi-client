from setuptools import setup, find_packages

setup(
    name="weatherpi",
    description="A weatherstation running on Raspberry Pi",
    authors=["Noah Biederbeck", "Maximilian Sackel"],
    license="MIT",
    version="0.0.2",
    packages=find_packages(),
)
