#!/bin/bash
source venv/weatherpi-client/bin/activate
pip install -e ./installation/.
pip install -r installation/requirements.txt
