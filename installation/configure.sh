echo ""
echo "-----------------------------------------------------"
echo "Wie ist dein Name?"
read user
echo "$user, wo steht deine Wetterstation?"
read location
echo "$user, deine Wetterstation steht in $location"

sed "s#insert_weatherpipath#$(pwd)#" installation/config.yml > config.yml
sed -i "s#insert_user#$user#" config.yml
sed -i "s#insert_location#$location#" config.yml
