echo ""
echo "+--------------------------------------------------------+"
echo "|Um die Wetterstation bei jedem Neustart des Raspberry Pi"
echo "|neu zu starten, gib folgendes Kommandos ein:            "
echo "|                                                        "
echo "|crontab -e                                              "
echo "|                                                        "
echo "|Und erstelle ein Zeile am Ende der Datei, in der steht  "
echo "|(ohne Anfuehrungszeichen ""):                           "
echo "|                                                        "
echo "|\"@reboot bash $(pwd)/bootscript.sh &\""
echo "+--------------------------------------------------------+"
echo ""
